package eu.bibl.updater.analysis.api;

public abstract interface ProviderInfo {
	
	public abstract int[] getProviderGameRevisions();
}