package eu.bibl.updaterimpl.rev170.analysers.network.packet.status.client;
public class C00PacketServerQueryAnalyser extends StatusPacketAnalyser {
	
	public C00PacketServerQueryAnalyser(ClassContainer container, HookMap hookMap) {
		super("C00PacketServerQuery", container, hookMap);
	}
	
	@Override
	public void run1() {
		
	}
}
